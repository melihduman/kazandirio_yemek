// -------------------------------------------------------------------------------------------------
// ### CONFIG
// -------------------------------------------------------------------------------------------------

// ## Global config
const ASSETS_PATH = 'src/assets/',
      BUILD_PATH = './dist/',
      DEV_PATH = './tmp/',

      // ## Static
      STATIC_PATHS = [ASSETS_PATH + 'media/**/*.*', ASSETS_PATH + 'fonts/**/*.*'],
      STATIC_BUILD_PATH = BUILD_PATH,
      STATIC_DEV_PATH = DEV_PATH,

      // ## Html
      HTML_PATH = 'src/views/',
      HTML_RENDER_PATH = HTML_PATH + 'pages/*.pug',
      HTML_WATCH_PATH = HTML_PATH + '**/*.pug',
      HTML_BUILD_PATH = BUILD_PATH,
      HTML_DEV_PATH = DEV_PATH,
      HTML_PRETTY = true,

      // ## Css
      CSS_PATH = 'styles/',
      CSS_ENRTY_PATH = ASSETS_PATH + CSS_PATH + 'app.scss',
      CSS_WATCH_PATH = ASSETS_PATH + CSS_PATH + '**/*.scss',
      CSS_BUILD_PATH = BUILD_PATH + ASSETS_PATH + CSS_PATH,
      CSS_DEV_PATH = DEV_PATH + ASSETS_PATH + CSS_PATH,
      CSS_AUTOPREFIXER_OPT = { cascade: false },

      // ## Scripts
      SCRIPTS_PATH = 'scripts/',
      SCRIPTS_ENRTY_PATH = ASSETS_PATH + SCRIPTS_PATH + 'app.js',
      SCRIPTS_WATCH_PATH = ASSETS_PATH + SCRIPTS_PATH + '**/*.js',
      SCRIPTS_BUILD_PATH = BUILD_PATH + ASSETS_PATH + SCRIPTS_PATH,
      SCRIPTS_DEV_PATH = DEV_PATH + ASSETS_PATH + SCRIPTS_PATH;

// -------------------------------------------------------------------------------------------------
// ### REQUER
// -------------------------------------------------------------------------------------------------

const path = require('path'),
      gulp = require('gulp'),
      clean = require('gulp-clean'),
      pug = require('gulp-pug'),
      sass = require('gulp-sass'),
      autoprefixer = require('gulp-autoprefixer'),
      cleanCss = require('gulp-clean-css'),
      sourcemaps = require('gulp-sourcemaps'),
      uglify = require('gulp-uglify'),
      include = require('gulp-include'),
      rename = require('gulp-rename'),
      replaceName = require('gulp-replace-name'),
      browserSync = require('browser-sync').create(),
      browserSyncReload = browserSync.reload;

// -------------------------------------------------------------------------------------------------
// ### PUBLIC TASKS
// -------------------------------------------------------------------------------------------------

// ## Serve
exports.serve = function(done) {
  return gulp.series(cleanBuild, cleanDev, (cleanDone) => {
    gulp.parallel(cssDev, scriptDev, htmlDev, copyStaticsDev, (devDone) => {
      gulp.watch(CSS_WATCH_PATH, cssWatch);
      gulp.watch(SCRIPTS_WATCH_PATH, scriptWatch);
      gulp.watch(HTML_WATCH_PATH, htmlWatch);
      gulp.watch(STATIC_PATHS, copyStaticsWatch);

      devDone();
      cleanDone();

      setTimeout(() => {
        browserSync.init({
          server: DEV_PATH
        });
      }, 100);
    })();

    done();
  })();
},


// ## Build
exports.build = function(done) {
  return gulp.series(cleanBuild, cleanDev, (cleanDone) => {
    gulp.parallel(cssBuild, scriptBuild, htmlBuild, copyStaticsBuild, (buildTaskDone) => {
      buildTaskDone();
      cleanDone();
    })();

    done();
  })();
}

// -------------------------------------------------------------------------------------------------
// ### CHILD TASKS
// -------------------------------------------------------------------------------------------------

// Css build
function cssBuild() {
  return gulp
    .src(CSS_ENRTY_PATH)
    .pipe(
      sass({
        includePaths: [
          path.join(__dirname, 'node_modules')
        ]
      }).on('error', sass.logError)
    )
    .pipe(autoprefixer(CSS_AUTOPREFIXER_OPT))
    .pipe(cleanCss())
    .pipe(rename({
      extname: '.min.css'
    }))
    .pipe(gulp.dest(CSS_BUILD_PATH));
}

// Css dev
function cssDev() {
  return gulp
    .src(CSS_ENRTY_PATH)
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        includePaths: [
          path.join(__dirname, 'node_modules')
        ]
      }).on('error', sass.logError)
    )
    .pipe(autoprefixer(CSS_AUTOPREFIXER_OPT))
    .pipe(sourcemaps.write())
    .pipe(rename({
      extname: '.min.css'
    }))
    .pipe(gulp.dest(CSS_DEV_PATH));
}

// Css watch
function cssWatch(cssWatchDone) {
  return gulp
    .series(cssDev, (cssDevDone) => {
      cssDevDone();
      cssWatchDone();
      browserSyncReload();
    })();    
};

// Script build
function scriptBuild(){
	return gulp.src(SCRIPTS_ENRTY_PATH)
		.pipe(include({
    		extensions: "js",
    		hardFail: true,
    		includePaths: [
          __dirname +'/node_modules',
          __dirname +'/'+ ASSETS_PATH+SCRIPTS_PATH
    		]
  		}))
  		.pipe(uglify())
      .pipe(rename({
        extname: '.min.js'
      }))
  		.pipe(gulp.dest(SCRIPTS_BUILD_PATH));
};

// Script dev
function scriptDev(){
	return gulp.src(SCRIPTS_ENRTY_PATH)
		.pipe(sourcemaps.init())
		.pipe(include({
    		extensions: "js",
    		hardFail: true,
    		includePaths: [
          __dirname +'/node_modules',
          __dirname +'/'+ ASSETS_PATH+SCRIPTS_PATH
    		]
  		}))
  		.pipe(sourcemaps.write())
      .pipe(rename({
        extname: '.min.js'
      }))
  		.pipe(gulp.dest(SCRIPTS_DEV_PATH));
};

// ## Script watch
function scriptWatch(scriptWatchDone) {
  return gulp
    .series(scriptDev, (scriptDevDone) => {
      browserSyncReload();
      scriptDevDone();
      scriptWatchDone();
    })();    
};

// Html build
function htmlBuild() {
	return gulp
    .src(HTML_RENDER_PATH)
    .pipe(replaceName(/\-p-/g, ''))
    .pipe(replaceName(/\home/g, 'index'))
		.pipe(pug({
			pretty:HTML_PRETTY
		}))
		.pipe(gulp.dest(HTML_BUILD_PATH));
} 

// Html dev
function htmlDev() {
  return gulp
    .src(HTML_RENDER_PATH)
    .pipe(replaceName(/\-p-/g, ''))
    .pipe(replaceName(/\home/g, 'index'))
    .pipe(pug({
      pretty:true
    }))
    .pipe(gulp.dest(HTML_DEV_PATH))
} 

// Html watch
function htmlWatch(htmlWatchDone) {
  return gulp
    .series(htmlDev, (htmlDevDone) => {
      browserSyncReload();
      htmlDevDone();
      htmlWatchDone();
    })();    
};


// Copy statics build
function copyStaticsBuild() {
  return gulp
    .src(STATIC_PATHS, { base: '.' })
    .pipe(gulp.dest(STATIC_BUILD_PATH));
}

// Copy statics dev.
function copyStaticsDev() {
  return gulp
    .src(STATIC_PATHS, { base: '.' })
    .pipe(gulp.dest(STATIC_DEV_PATH));
}

// Copy statics watch
function copyStaticsWatch(copyStaticsWatchDone) {
  return gulp
    .series(copyStaticsDev, (copyStaticsDevDone) => {
      browserSyncReload();
      copyStaticsDevDone();
      copyStaticsWatchDone();
    })();    
};

// Clean dev.
function cleanDev() {
  return gulp
    .src(DEV_PATH, {allowEmpty: true})
    .pipe(clean({force:true}));
}

// Clean build.
function cleanBuild() {
  return gulp
    .src(BUILD_PATH, {allowEmpty: true})
    .pipe(clean({force:true}));
}
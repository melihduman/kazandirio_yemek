//=require jquery/dist/jquery.js
//=require select2/dist/js/select2.js
//=require owl.carousel/dist/owl.carousel.js

//=require helpers/ajax.js
//=require helpers/others.js

//=require controllers/search-filter.js 
//=require controllers/result-filter.js
//=require controllers/restaurant-profile.js

var APP = (function () {

  function init() {
    var $body = $('body'),
      helpers = new Helpers;
    page = $body.attr('id');

    var searchFilter = new SearchFilter;
    searchFilter.init($('.search-filter'));

    var resultFilter = new ResultFilter;
    resultFilter.init($('.result-filter'));

    var restaurantProfile = new RestaurantProfile;
    restaurantProfile.init($('.restaurant-profile__card'));

    if (helpers.isMobile()) {
      $body.addClass('touch-device');
    }
  }

  return {
    init: init
  }
});

$(document).ready(function () {
  var App = new APP;
  App.init();
});

$('.loop').owlCarousel({
  center: true,
  items: 1,
  loop: false,
  stagePadding:50,
  responsive: {
    350: {
      items: 1.2
    },
    400: {
      stagePadding: 100,
      margin:1,
      center:false,
      items: 1,
    },
    500: {
      stagePadding: 1,
      margin:5,
      autoWidth:true,
      center:false,
      responsive:true,
      items: 2.25
    },
    600: {
      stagePadding: 1,
      margin:5,
      autoWidth:true,
      center:false,
      responsive:true,
      items: 2.25
    },
    700: {
      responsive:true,
      items:3,
    },
    900: {
      responsive:true,
      items:3,
    }
  }
});
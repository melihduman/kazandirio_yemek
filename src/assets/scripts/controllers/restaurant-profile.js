RestaurantProfile = (function() {
  var $restaurantProfileEl,
      $restaurantProfileImage,
      $restaurantProfileCard,
      $clickToCall,
      $clickToNav,
      ajaxAnalyticsPhone = window.AJAX.analyticsPhone;
      ajaxAnalyticsNavigateMap = window.AJAX.analyticsNavigateMap;
      ajax = new Ajax;

  // Init.
  function init($el) {
    $restaurantProfileEl = $el;

    if($restaurantProfileEl.length != 0){
      $restaurantProfileEl.each(function () {
        restaurantProfileSetup($(this));
      });
    }
  }

  // Search filter setup.
  function restaurantProfileSetup($el) {
    $restaurantProfileCard = $el.find('.card__title');
    $restaurantProfileImage = $el.find('.card__profile-image')
    $clickToCall = $el.find('.card__button--call a');
    $clickToNav = $el.find('.card__button--geo a');

    $restaurantProfileImage.on('click', function() {
      if(!$el.hasClass('restaurant-profile__card--closed')) {
        $el.toggleClass('restaurant-profile__card--expanded');
      }
    });

    $restaurantProfileCard.on('click', function() {
      if(!$el.hasClass('restaurant-profile__card--closed')) {
        $el.toggleClass('restaurant-profile__card--expanded');
      }
    });

    $clickToCall.on('click', function(e) {
      if($(this).hasClass('button--disabled')) {
        e.preventDefault();
        return false;
      }

      var phone = $(this).data('phone')
      var marketId = $(this).data('market-id');
      var marketName = $(this).data('market-name');
      var marketCity = $(this).data('market-city');
      var marketLocality = $(this).data('market-locality');

      //ga('send', 'event', 'Restaurant Detail', 'Call', marketId);
      gtagClick('Restaurant-Call',marketName, marketCity, marketLocality);

      ajax.request(
        'POST',
        ajaxAnalyticsPhone+'/'+marketId,
        null,
        null,
        null,
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
      )

      window.location = 'tel:'+phone;
    });

    $clickToNav.on('click', function(e) {
      if($(this).hasClass('button--disabled')) {
        e.preventDefault();
        return false;
      }


      var lat = $(this).data('latitude');
      var long = $(this).data('longitude');
      var fromLat = $(this).data('from-latitude');
      var fromLong = $(this).data('from-longitude')
      var marketId = $(this).data('market-id');
      var marketName = $(this).data('market-name');
      var marketCity = $(this).data('market-city');
      var marketLocality = $(this).data('market-locality');
      var daddr = '';
      var saddr = '';
      //ga('send', 'event', 'Restaurant Detail', 'Navigate', resaurantId);
  
      gtagClick('Restaurant-Map',marketName, marketCity, marketLocality);

      ajax.request(
        'POST',
        ajaxAnalyticsNavigateMap+'/'+marketId,
        null,
        null,
        null,
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
      )

      if (fromLat && fromLong) {
        saddr= '&saddr='+fromLat+','+fromLong;
      }

      daddr = '?daddr='+lat+','+long;

      if (
        (navigator.platform.indexOf("iPhone") != -1) || 
        (navigator.platform.indexOf("iPad") != -1) || 
        (navigator.platform.indexOf("iPod") != -1)
      ) { 
        window.open("maps://maps.google.com/maps"+daddr+saddr);
      } else {
        window.open("https://maps.google.com/maps"+daddr+saddr);
      }
    });
  }

  return {
    init:init
  }
});
var SearchFilter = (function() {

  var $searchFilterEl,
      $searchFilterCitySelector,
      $searchFilterDistrictSelector,
      ajax = new Ajax;
      ajaxPending = false;
      ajaxURL = window.AJAX.district;
      language = {
        errorLoading: function() {
          return "Sonuç yüklenemedi"
        },
        inputTooLong: function(n) {
            return n.input.length - n.maximum + " karakter daha girmelisiniz"
        },
        inputTooShort: function(n) {
            return "En az " + (n.minimum - n.input.length) + " karakter daha girmelisiniz"
        },
        loadingMore: function() {
            return "Daha fazla…"
        },
        maximumSelected: function(n) {
            return "Sadece " + n.maximum + " seçim yapabilirsiniz"
        },
        noResults: function() {
            return "Sonuç bulunamadı"
        },
        searching: function() {
            return "Aranıyor…"
        },
        removeAllItems: function() {
            return "Tüm öğeleri kaldır"
        }
      }


  // Init.
  function init($el) {
    $searchFilterEl = $el;

    if($searchFilterEl.length != 0){
      $searchFilterEl.each(function () {
        searchFilterSetup($(this));
      });
    }
  }

  // Search filter setup.
  function searchFilterSetup($el) {
    $searchFilterCitySelector = $el.find('.city__selector');
    $searchFilterDistrictSelector = $el.find('.district__selector');
    $searchErrorMessage = $el.find('.search-filter__error');

    // City selector setup.
    $searchFilterCitySelector.select2({
      placeholder: 'Şehir seç',
      language: language, 
    });

    function showHideErrorMessage() {
      if(
        $searchFilterCitySelector.hasClass('select2--error') 
        || $searchFilterDistrictSelector.hasClass('select2--error')
      ) {
        $searchErrorMessage.removeClass('search-filter__error--hide');
      } else {
        $searchErrorMessage.addClass('search-filter__error--hide');
      }
    };

    citySelectorChange($searchFilterCitySelector.val(), window.AJAX.localityId);

    // City selector change event.
    $searchFilterCitySelector.change(function(e) {
      if(!$(this).val()) {
        if(!$(this).hasClass('select2--error')) {
          $(this).addClass('select2--empty');
        }
      } else {
        $(this).removeClass('select2--error').removeClass('select2--empty');
      }

      showHideErrorMessage();
      citySelectorChange($(this).val());
    })

    // District selector setup
    $searchFilterDistrictSelector.select2({
      placeholder: 'İlçe/semt seç',
      language: language, 
    });

    $searchFilterDistrictSelector.change(function(e) {
      if(!$(this).val() || window.AJAX.localityId=='error') {
        if(!$(this).hasClass('select2--error')) {
          $(this).addClass('select2--empty');
        }
      } else {
        $(this).removeClass('select2--error').removeClass('select2--empty');
      }
      if (window.AJAX.localityId=='error') window.AJAX.localityId='';
      showHideErrorMessage();
    });
  }

  // City selector.
  function citySelectorChange(val, localityId) {
    ajaxPending = true;
    var sendFormDone = function(d) {
          // Parse vars.
          var district = JSON.stringify(d).split(',');
          var options = [];

          // Parse ajax data.
          district.forEach(function(v) {
              if (v.trim() != '[]') {
                  var idAndName = v.replace('"','').replace('"','').replace('{', '').split(':');
                  var districtAndState = idAndName[0].split('-');

                  options.push({
                      id: parseInt(idAndName[1]),
                      text: districtAndState[0]+'-'+districtAndState[1]
                  })
              }
          }),

          // Remove old options.
          $searchFilterDistrictSelector.html('').trigger('change');

          //let allSelected = isEmpty(window.AJAX.localityId) || window.AJAX.localityId == 'all' ? true : false;
          // Select empty.
          // $searchFilterDistrictSelector.append(new Option('TÜMÜ', null, true, allSelected)).trigger('change');

            let isAnyDistrictSelected = false;
            // Append new data.
            options.forEach(function(o) {
              let isSelected =  localityId == o.id ? true : false;
              if (isSelected) isAnyDistrictSelected = true;
              //$searchFilterDistrictSelector.append(new Option(o.text, o.id, false, isSelected)).trigger('change');
              $searchFilterDistrictSelector.append(new Option(o.text, o.id, false, isSelected));
            })

            if (!isAnyDistrictSelected) $searchFilterDistrictSelector.val(null);

            $searchFilterDistrictSelector.trigger('change');

          // Ajax end.
          ajaxPending = false;
        },
        senFormFail = function(e) {
          ajaxPending = false;
        };

    if (val) {
        ajax.request('GET', ajaxURL+'/'+val, null, sendFormDone, senFormFail);
    }
  };

  return {
    init:init
  }
});
ResultFilter = (function() {
    var $resultFilterEl,
        $resultFilterForm,
        $resultFilterClicks,
        $resultFilterInput,
        $dropDownSelected;

    // Init.
    function init($el) {
        $resultFilterEl = $el;
        $resultFilterForm = $('#result-filter');
        $resultFilterClicks = $resultFilterEl.find('a');
        $resultFilterInput = $resultFilterEl.find('input')
        $dropDown = $resultFilterEl.find('.options__dropdown');
        $dropDownSelected = $resultFilterEl.find('.dropdown__selected');
        $dropDownSelectedText = $resultFilterEl.find('.dropdown__selected span')

        if($resultFilterClicks.length != 0){
            $resultFilterClicks.each(function () {
                resultFilterClickSetup($(this));
            });
        }

        $dropDownSelected.on('click', function() {
            $dropDown.toggleClass('options__dropdown--opened');
        });

        $resultFilterForm.on('submit', function() {
            sendFormToGtag();
        });
    }

    function sendFormToGtag() {
        let city = $('#city').val();
        let keywords = $('#keywords').val(); // ''=boş
        let localityId = $('#localityId').val(); //TÜMÜ = null
        let localityName = $('#localityId option:selected').text();
        let sortType =  $('#sortType').val();
        let isKw = keywords.trim()!='';

        if (!isEmpty(city) && !isEmpty(localityId)) {
            gtagSearch('Search', city, localityName, keywords.trim());
        }

        if (isEmpty(city)) {
            gtagGeneric('Search-City-Error','error');
        }

        if (isEmpty(localityId)) {
            gtagGeneric('Search-Locality-Error', !isEmpty(city) ? city : 'error');
        }

        if (isKw) gtagGeneric('Search-Keyword',keywords.trim());

        if (!isEmpty(sortType)) gtagGeneric('Search-SortType',sortType);

    }

    // Result filter click setup.
    function resultFilterClickSetup($el) {
        if(!$el.hasClass('button--angular-passive')) {
            $resultFilterInput.val($el.data('value'));
            $dropDownSelectedText.text($el.text());
        }

        $el.on('click', function() {
            if($el.hasClass('button--angular-passive')) {
                $resultFilterClicks
                    .addClass('button--angular-passive');
                $resultFilterInput.val($(this).data('value'));
                $dropDown.toggleClass('options__dropdown--opened');
                $resultFilterForm.submit();
                $dropDownSelectedText.text($el.text());
                $(this).removeClass('button--angular-passive');
            }
        });
    }

    return {
        init:init
    }
});
var Helpers = (function () {
  function isMobile() {
    if( navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
    ){
        return true;
    }
    else {
        return false;
    }
  }

  return {
    isMobile:isMobile,
  }
});

function gtagGeneric(action, value) {
  var objArr = {};
  objArr['SOURCE'] = window.AJAX.gaSOURCE;
  objArr['UDID'] = window.AJAX.gaUDID;
  objArr[value] = 1;
  
  //Global GTM tarafında yönetileceği için commentlendi
  //gtag('event', action, objArr);
}

function gtagClick(action, marketName, city, locality) {
  var objArr = {};
  //objArr[window.AJAX.gaSOURCE] = 1;
  objArr['SOURCE'] = window.AJAX.gaSOURCE;
  objArr['UDID'] = window.AJAX.gaUDID;
  objArr['CITY'] = city;
  objArr['LOCALITY'] = locality;
  objArr['CITY-LOCALITY'] = city + '-' + locality;
  //objArr[marketName] = 1;
  objArr['RESTAURANT'] = marketName;

  //Global GTM tarafında yönetileceği için commentlendi
  //gtag('event', action, objArr);

}
function gtagSearch(action, city, locality, kw) {
  var objArr = {};
  //objArr[window.AJAX.gaSOURCE] = 1;
  objArr['SOURCE'] = window.AJAX.gaSOURCE;
  objArr['UDID'] = window.AJAX.gaUDID;
  objArr['CITY'] = city;
  //objArr['LOCALITY'] = locality;
  objArr['CITY-LOCALITY'] = city + '-' + locality;
  objArr[city] = (kw==null ? 'empty-kw' : kw);
  objArr[city + '-' + locality] = (kw==null ? 'empty-kw' : kw)

  //Global GTM tarafında yönetileceği için commentlendi
  //gtag('event', action, objArr);

}


function isEmpty(str) {
  return str == undefined || str==null || str=='null' || str.trim()=='';
}
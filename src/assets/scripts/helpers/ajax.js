var Ajax = (function () {

  function request(method, service, data, done, error, headers) {
      var _ajaxContent = {
          url: service,
          data: data,
          cache: false,
          dataType: 'json',
          processData: false,
          contentType: false,
          type: method,
          success: done,
          error: error,
          headers: headers,
      };

      $.ajax(_ajaxContent);
  }

  return {
      request: request
  }
});